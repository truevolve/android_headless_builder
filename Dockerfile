FROM ubuntu:22.04

# Declare this first so docker doesn't need to rerun it when changing Android version numbers
RUN apt-get update \
    	&& \
    		apt-get install -y --no-install-recommends \
			curl \
			wget \
			lib32stdc++6 \
			lib32z1 \
			lib32ncurses6 \
			openjdk-17-jdk \
			unzip \
			git \
    	&& \
      		rm -rf /var/lib/apt/lists/ \
    	&& \
		apt-get clean

ENV ANDROID_COMPONENTS_PFM "platform-tools"
ENV ANDROID_HOME /usr/local/android-sdk
ENV ANDROID_NDK_HOME ${ANDROID_HOME}/ndk-bundle
ENV GRADLE_FOLDER /root/.gradle

# Install Android tools
RUN mkdir -p /usr/local/android-sdk/licenses/ && touch /usr/local/android-sdk/licenses/android-sdk-license
RUN echo "24333f8a63b6825ea9c5514f83c2829b004d1fee" > /usr/local/android-sdk/licenses/android-sdk-license

# Set up Gradle
RUN mkdir ${GRADLE_FOLDER}
RUN echo "allprojects {\n    repositories {\n        mavenLocal()\n            maven {\n	        credentials {\n	            username travenUser\n	            password travenPassword\n\
            }\n            url \"https://truevolve-traven.appspot.com\"\n        }\n    }\n}\n" > ${GRADLE_FOLDER}/init.gradle

RUN echo "travenUser=REPLACE_THIS_WITH_YOUR_USERNAME\ntravenPassword=REPLACE_THIS_WITH_YOUR_PASSWORD\norg.gradle.caching=true" > ${GRADLE_FOLDER}/gradle.properties

# Add android commands to PATH
ENV PATH $PATH:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools:${ANDROID_HOME_NDK}

# Export JAVA_HOME variable
ENV JAVA_HOME /usr/lib/jvm/java-17-openjdk-amd64/

ENV TERM dumb

# Get the latest version from https://developer.android.com/studio/index.html
ENV ANDROID_SDK_TOOLS_VERSION "10406996_latest"

ENV ANDROID_COMPONENTS_SDK "platforms;android-23 platforms;android-24 platforms;android-25 platforms;android-26 platforms;android-27 platforms;android-28 platforms;android-29 platforms;android-30 platforms;android-31 platforms;android-32 platforms;android-33"
ENV ANDROID_COMPONENTS_BDT "build-tools;25.0.3 build-tools;26.0.1 build-tools;27.0.3 build-tools;28.0.3 build-tools;29.0.3 build-tools;30.0.1 build-tools;31.0.0 build-tools;32.0.0 build-tools;33.0.2"

ENV ANDROID_NDK_COMPONENTS "ndk-bundle" \
                       "cmake;3.10.2.4988404"
    
# Install Android SDK
RUN wget --quiet -O android-sdk.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS_VERSION}.zip \
	&& \
		unzip -q android-sdk.zip -d android-sdk-linux \
	&& \
		mv android-sdk-linux/cmdline-tools ${ANDROID_HOME}/cmdline-tools \
	&& \
		rm android-sdk.zip

# RUN ln -s ${ANDROID_HOME}/cmdline-tools ${ANDROID_HOME}/tools

RUN ${ANDROID_HOME}/cmdline-tools/bin/sdkmanager ${ANDROID_COMPONENTS_PFM} --include_obsolete --sdk_root=${ANDROID_HOME} --verbose
RUN ${ANDROID_HOME}/cmdline-tools/bin/sdkmanager ${ANDROID_COMPONENTS_SDK} --include_obsolete --sdk_root=${ANDROID_HOME} --verbose
RUN ${ANDROID_HOME}/cmdline-tools/bin/sdkmanager ${ANDROID_COMPONENTS_BDT} --include_obsolete --sdk_root=${ANDROID_HOME} --verbose
RUN ${ANDROID_HOME}/cmdline-tools/bin/sdkmanager ${ANDROID_NDK_COMPONENTS} --include_obsolete --sdk_root=${ANDROID_HOME} --verbose


CMD /bin/bash
